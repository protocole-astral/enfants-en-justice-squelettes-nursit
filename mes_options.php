<?php
// active HSTS, cf. https://scotthelme.co.uk/issuing-hsts-policy-in-php/
header("strict-transport-security: max-age=600");
// désactiver l'héritage des logos de rubriques
define('_LOGO_RUBRIQUE_DESACTIVER_HERITAGE', true);
