Installation en local :

- suivre l'option 1 de cette page : https://www.spip.net/fr_article3141.html

Modifications à faire dans SPIP :

- ajout d'une image et d'une description dans la rubrique "Visite" (1)
- copie du champ "Texte" de l'article "De quoi s’agit-il ?" (144) vers le champ "Texte explicatif" de la rubrique "Visite" (1)
- copie du champ "PS" de l'article "Plan interactif" (172) dans le champ "Texte explicatif" de la rubrique "Visite virtuelle" (5)
- ajout d'une image et d'une description dans la rubrique "Thème" (11)
- copie du champ "Texte" de l'article "De quoi s’agit-il ?" (1) dans le champ "Texte explicatif" de la rubrique "Thèmes" (11)
- modification du champ "À l’intérieur de la rubrique" de la rubrique "Thème" (11) pour choisir "Racine du site"
- renommage de la rubrique "Portail" (2) en "En Une"
- renommage de la rubrique "Thème" (11) en "Portail"
- modification du champ "À l’intérieur de la rubrique" de la rubrique "Actualités du portail" (12) pour choisir "Racine du site"
- création du groupe de mot-clés "Images", l'associer aux documents, création du mot-clé "HD", l'ajouter sur des images
