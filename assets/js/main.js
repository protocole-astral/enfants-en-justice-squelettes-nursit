// SHARE BUTTON SCRIPT

const shareData = {
  title: document.title,
  /*text: 'Learn web development on MDN!',*/
  url: window.location.href
}

if (document.getElementById("share_button")) {

  const btn = document.getElementById("share_button").parentNode;
  const resultPara = btn;

  // Share must be triggered by "user activation"
  btn.addEventListener('click', function(){
    try {
      navigator.share(shareData);
    } catch (err) {
      navigator.clipboard.writeText(shareData.url).then(
        () => {
          resultPara.children[1].textContent = "Lien copié !";
          setTimeout(function(){
            this.textContent = "Partager cette page";
          }.bind(resultPara.children[1]),1000);
        },
        () => {
          resultPara.children[1].textContent = "Le lien n'a pas pu être copié";
          setTimeout(function(){
            this.textContent = "Partager cette page";
          }.bind(resultPara.children[1]),1000);
        }
      );
    }
  });
}


// urls Adolie
var url_api_adolie = "https://adolie.enpjj.justice.fr/api/items?itemtype_id=1";
var url_permalien_adolie = "https://adolie.enpjj.justice.fr/idurl/1/";
var url_img_adolie = "https://adolie.enpjj.justice.fr/i/";

function getURLSearchParams() {
    /*
        Renvoie les paramètres URL
    */
    var url = new URL(document.location.href);
    var searchParams = new URLSearchParams(url.search);
    return searchParams;
}

async function getData(path) {
    /*
        Fait une requete sur path
        et renvoie la réponse JSON 
    */
    var options = {
        method: 'GET',
        mode: "cors"
    };
    var response = await fetch(path, options);
    var text = await response.text();
    var json = await JSON.parse(text);
    return json;
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}

// PLAN INTERACTIF RESPONSIVE

window.addEventListener("load",function(e){
  let plan = document.getElementById("svg13103");
  if (plan && window.innerWidth < 1000) {
    plan.setAttribute("viewBox","60 25 195 120");
  }  
});
