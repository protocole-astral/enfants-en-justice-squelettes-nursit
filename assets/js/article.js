document.addEventListener("DOMContentLoaded", function() {
    initArticle();
}, false);

function initArticle() {
    // click sur les images HD
    var figures_hd = document.querySelectorAll(".show_big_image");
    figures_hd.forEach(figure => figure.addEventListener("click", handleFigureClick));
}

function displayZoomFigure(figure) {
    var clone = figure.cloneNode(true);

    var body = document.querySelector("body");
    var zoom_container = document.createElement("DIV");
    zoom_container.id = "zoom_container";

    var close_button = document.createElement("BUTTON");
    close_button.id = "close_button";
    close_button.addEventListener("click", handleButtonCloseClick);

    // remplacement du src d'origine (petite image) par le src "data-large"
    var src_large = clone.querySelector("img").getAttribute("data-large");
    clone.querySelector("img").setAttribute("src", src_large);

    // suppr ancien bouton
    clone.removeChild(clone.querySelector(".show_big_image"));

    clone.appendChild(close_button);
    zoom_container.appendChild(clone);
    body.appendChild(zoom_container);

    // ajout event pour touche "echap"
    document.addEventListener('keydown', handleKeyboardInput);
}
function removeZoomFigure() {
    var zoom_container = document.querySelector("#zoom_container");
    if (zoom_container) {
        zoom_container.parentNode.removeChild(zoom_container);
        // retrait event pour touche "echap"
        document.removeEventListener('keydown', handleKeyboardInput);
    }
}

function handleFigureClick() {
    displayZoomFigure(this.parentNode);
}
function handleButtonCloseClick () {
    removeZoomFigure();
}
function handleKeyboardInput() {
    if (event.key == "Escape") {
        removeZoomFigure();
    }
}