document.addEventListener("DOMContentLoaded", function() {
    initSommaire();
}, false);

async function initSommaire() {


    searchAdolie();
}

async function searchAdolie() {
    /* 
        Fait une requête sur l'API Adolie
        et affiche la liste des résultats
    */

    var api_offsets = [0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000];
    var random_page_offset = api_offsets[getRandomInt(0, 10)];
    var random_item_offset = getRandomInt(0, 95);

    // requête API Adolie
    var request = url_api_adolie + "&limit=100&offset=" + random_page_offset;
    console.log(request);
    var data = await getData(request);
    console.log(data);

        // div pour ajouter la liste des résultats
        var content = document.querySelector(".bloc.summary-children.adolie .content");

        // boucle dans 4 résultats
        for (var i = random_item_offset; i < random_item_offset + 4; i++) {
            if (data.items[i]) {

                // pr chaque résultat : recup url + titre + image
                var item = data.items[i];
                var item_url = url_permalien_adolie + item["_id"];
                var item_title = item["_source"]["Title"][0];
                var item_img = item["_source"]["uuid_to_media_path"];

                // clone du template (cf. html) pour l'adapter
                var clone = document.querySelector(".bloc_summary_sub_children_template").content.cloneNode(true);
                clone.querySelector("a").href = item_url;
                clone.querySelector("h4").innerHTML = item_title;
                clone.querySelector("img").src = url_img_adolie + item_img;

                // ajout dans le DOM
                content.appendChild(clone);
            }
        }
    
}
