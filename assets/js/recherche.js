document.addEventListener("DOMContentLoaded", function() {
    initRecherche();
}, false);

async function initRecherche() {

    // requête sur l'API Adolie
    searchAdolie();
}

// urls Adolie
var url_api_adolie = "https://adolie.enpjj.justice.fr/api/items?itemtype_id=1&search=";
var url_permalien_adolie = "https://adolie.enpjj.justice.fr/idurl/1/";
var url_img_adolie = "https://adolie.enpjj.justice.fr/i/";

async function searchAdolie() {
    /* 
        Fait une requête sur l'API Adolie
        et affiche la liste des résultats
    */

    // recup des params urls
    var searchParams = getURLSearchParams();

    if (searchParams.has("recherche")) {
        // si url a le param "recherche"
        var search_string = searchParams.get("recherche");

        // requête API Adolie avec la string de recherche
        var request = url_api_adolie + "&search=" + search_string;
        var data = await getData(request);

        // affiche le nombre total de résultats (sans la pagination) 
        var result_count = document.querySelector(".result_count");
        result_count.innerHTML = data["_meta"].total;

        // div pour ajouter la liste des résultats
        var content = document.querySelector(".recherche.adolie .content .content");

        // boucle dans les 5 premiers résultats
        for (var i = 0; i < 5; i++) {
            if (data.items[i]) {

                // pr chaque résultat : recup url + titre + image
                var item = data.items[i];
                var item_url = url_permalien_adolie + item["_id"];
                var item_title = item["_source"]["Title"][0];
                var item_img = item["_source"]["uuid_to_media_path"];

                // clone du template (cf. html) pour l'adapter
                var clone = document.querySelector(".bloc_summary_sub_children_template").content.cloneNode(true);
                clone.querySelector("a").href = item_url;
                clone.querySelector("h4").innerHTML = item_title;
                clone.querySelector("img").src = url_img_adolie + item_img;

                // ajout dans le DOM
                content.appendChild(clone);
            }
        }

        // si pas de résultats dans Adolie
        if (!data.items.length) {
            var p = document.createElement("P");
            p.innerHTML = "Pas de résultats dans Adolie pour cette recherche.";
            content.appendChild(p);
        }
    }
}
